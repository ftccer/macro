import 'package:flutter/material.dart';
import 'bluetooth.dart';
import 'dart:async';

class Start extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return MaterialApp(
      title: 'Start',
      home: Startseite(),
    );
  }
}


class Startseite extends StatefulWidget {
    @override
  State createState() => new StartseiteState();
}
class StartseiteState extends State<Startseite> {
  @override
  Widget build(BuildContext context) {

    return Scaffold(
      body:
       Container(
        decoration: BoxDecoration(
          image: new DecorationImage(
            image: AssetImage("assets/animal-bee.jpg"),
            fit: BoxFit.cover,
          ),
        ),
        child: Container(
          decoration: BoxDecoration(

              color: Colors.black26.withOpacity(0.2),
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              SizedBox(height: 0.0,),
              Center(
                child: Container(
                  decoration: BoxDecoration(
                    color: Colors.white70.withOpacity(0.3)
                  ),
                  padding: const EdgeInsets.only(left: 38.0, right: 38.0, top: 120.0, bottom: 40.0),
                  child: Text("Steuere deine Makrofotografie Projekte kinderleicht mit der Macro App "
                      .toUpperCase(),
                    style: TextStyle(color: Colors.brown, fontSize: 20.0, fontWeight: FontWeight.w600),
                    textAlign: TextAlign.center,
                  ),
                )
              ),
              Spacer(),
              Container(
                  margin: EdgeInsets.symmetric(horizontal: 10.0, vertical: 40.0),
                  decoration: BoxDecoration(

//                    border: Border.all(width: 1.0, style: BorderStyle.solid, color: Colors.black),
                    borderRadius: BorderRadius.circular(10.0)
                  ),
                  child: RaisedButton.icon(
                    color: Colors.brown,

                    label: Text("weiter".toUpperCase(), style: TextStyle(color: Colors.white, fontSize: 14.0), ),
                    icon: Icon(Icons.chevron_right, color: Colors.white),
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => BluetoothRoute()),
                      );
                    }
                  ),
              ),

            ],
          ),
        ),
      ),
    );
  }


}