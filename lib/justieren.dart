import 'package:flutter/material.dart';
import 'bluetooth.dart';
import 'dart:async';
import 'package:flutter_bluetooth_serial/flutter_bluetooth_serial.dart';


class Setup extends StatelessWidget {

  final BluetoothDevice device;
  FlutterBluetoothSerial bluetooth;
  Setup({Key key, @required this.device, @required this.bluetooth}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return MaterialApp(
      title: 'Setup',
      home: Body(device,bluetooth),
    );
  }
}

class Body extends StatefulWidget {

  BluetoothDevice _device;
  FlutterBluetoothSerial bluetooth;


  Body(BluetoothDevice device, this.bluetooth) {
    this._device = device;
    this.bluetooth = bluetooth;
  }

  State createState() => new BodyState(device:_device, bluetooth:bluetooth);
}

class BodyState extends State<Body> {
  bool btPressed = true;
  List<String> items = new List<String>();
  final BluetoothDevice device;
  FlutterBluetoothSerial bluetooth;
  String _value = "1.8";
  final TextEditingController _schrittController = new TextEditingController();
  final TextEditingController _anzahlController = new TextEditingController();
  final TextEditingController _controller = new TextEditingController();
  final ScrollController _scrollController = new ScrollController();
  int schritte = 0;
  String output = "";


  BodyState({@required this.device, @required this.bluetooth});

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _connect();
    reciveData();

  }




  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      body: Container(
        child: _innerBody(),
        decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage("assets/animal-bee.jpg"),
            )
        ),
      ),
    );
  }
  Widget _innerBody() {
    _controller.text = schritte.toString();
    return Container(
      decoration: BoxDecoration(
        color: Colors.white70.withOpacity(0.8),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Center(
            child: Container(
              padding: EdgeInsets.only(left: 40.0, top: 80.0, bottom: 20.0),
              child: Row(
                children: <Widget>[
                  Text("Justieren", style: TextStyle(color: Colors.white, fontSize: 28.0, height: 2.0),),
                ],
              ),
              decoration: BoxDecoration(
                color: Colors.brown.withOpacity(0.9),
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.symmetric(vertical: 10.0),
            padding: EdgeInsets.only(bottom: 20.0),
            decoration: BoxDecoration(
              color: Colors.white70.withOpacity(0.8),
            ),
            child: Column(
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.all(18.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text("Winkel".toUpperCase(), style: TextStyle(color: Colors.grey, fontSize: 15.0, fontWeight: FontWeight.w500),),
                          Container(
                            margin: EdgeInsets.only(top: 5.0),
                            padding: EdgeInsets.symmetric(vertical: 0.0, horizontal: 10.0),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(5.0),
                              color: Colors.grey,
                            ),
                            child: DropdownButtonHideUnderline(
                              child: DropdownButton<String>(
                                items: <String>["1.8","0.9","0.45","0.225", "0.1125"].map((String value) {
                                  return new DropdownMenuItem<String>(
                                    value: value,
                                    child: Row(
                                      children: <Widget>[
                                        SizedBox(width: 30.0,),
                                        Text(value),
                                      ],
                                    ),
                                  );
                                }).toList(),
                                onChanged: (value) {
                                  setState(() => {
                                  _value = value
                                  });
                                },
                                value: _value,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(15.0),
                      child: Column(

                        children: <Widget>[
                          Text("BildMenge".toUpperCase(), style: TextStyle(color: Colors.grey, fontSize: 15.0, fontWeight: FontWeight.w500),),
                          Container(
                            width: 80.0,
                            margin: EdgeInsets.only(top: 5.0),
                            padding: EdgeInsets.symmetric(vertical: 0.0, horizontal: 10.0),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(5.0),
                              color: Colors.grey.withOpacity(0.6),
                            ),
                            child: TextField(
                              controller: _anzahlController,
                              onSubmitted: _submit,
                              keyboardType: TextInputType.number,
                              decoration: InputDecoration(
                                  border: UnderlineInputBorder(borderSide: BorderSide(width: 0.0, style: BorderStyle.none)),
                                  labelStyle: TextStyle(color: Colors.black87)),
                            ),

                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(15.0),
                      child: Column(

                        children: <Widget>[
                          Text("Schritte".toUpperCase(), style: TextStyle(color: Colors.grey, fontSize: 15.0, fontWeight: FontWeight.w500),),
                          Container(
                            width: 80.0,
                            margin: EdgeInsets.only(top: 5.0),
                            padding: EdgeInsets.symmetric(vertical: 0.0, horizontal: 10.0),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(5.0),
                              color: Colors.grey.withOpacity(0.6),
                            ),
                            child: TextField(
                              controller: _schrittController,
                              onSubmitted: _submit,
                              keyboardType: TextInputType.number,
                              decoration: InputDecoration(
                                  border: UnderlineInputBorder(borderSide: BorderSide(width: 0.0, style: BorderStyle.none)),

                                  labelStyle: TextStyle(color: Colors.black87)),
                              /*onChanged: (text) {
                                _schritte= text;
                              },*/
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,

                  children: <Widget>[
                    Text("Justieren".toUpperCase(), style: TextStyle(color: Colors.black45, fontSize: 18.0, fontWeight: FontWeight.w500),),
                    SizedBox(width: 20.0,),
                    Container(
                      decoration: BoxDecoration(color: Colors.grey, borderRadius: BorderRadius.all(Radius.circular(2.0))),
                      padding: const EdgeInsets.symmetric(vertical: 6.0, horizontal: 6.0),
                      child: InkWell(
                        onTap: _decrease,
                        child: Icon(Icons.remove, color: Colors.white,),
                      ),
                    ),
                    Container(
                      width: 50.0,
                      padding: EdgeInsets.symmetric(horizontal: 2.0),
                      decoration: BoxDecoration(color: Colors.white.withOpacity(0.7), borderRadius: BorderRadius.all(Radius.circular(2.0)) ),
                      child: TextField(
                        keyboardType: TextInputType.number,
                        style: TextStyle(fontSize: 15.0, height: 1.4),
                        textAlign: TextAlign.center,
                        controller: _controller,
                        onChanged: (text){ schritte = int.tryParse(text); },
                        onSubmitted: _submit,
                        decoration: InputDecoration(
                            contentPadding: EdgeInsets.symmetric(vertical: 6.0),

                            border: UnderlineInputBorder(borderSide: BorderSide(width: 0.0, style: BorderStyle.none)),
                            labelStyle: TextStyle(color: Colors.black)),
                      ),
                    ),
                    Container(
                      decoration: BoxDecoration(color: Colors.grey, borderRadius: BorderRadius.all(Radius.circular(2.0))),
                      padding: const EdgeInsets.symmetric(vertical: 6.0, horizontal: 6.0),

                      child: InkWell(
                        onTap: _increase,
                        child: Icon(Icons.add, color: Colors.white,),
                      ),
                    ),
                    SizedBox(width: 15.0,),

                    SizedBox(
                      width: 55.0,
                      child: RaisedButton(
                        onPressed: (){
                          setState(() {
                            if(schritte > 0 && _schrittController.text.isNotEmpty){
                              bluetooth.write("{\"paused\":true,\"vor\":" + schritte.toString() + "}");
                              if(btPressed) btPressed = false;
                            }
                            else if(schritte < 0 && _schrittController.text.isNotEmpty){
                              bluetooth.write("{\"paused\":true,\"rueck\":" + (schritte * -1).toString() + "}");
                              if(btPressed) btPressed = false;
                            }
                            schritte = 0;
                          });

                        },
                        child: Row(
                          mainAxisSize: MainAxisSize.min,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Text("OK"),
//                                Icon(Icons.play_arrow, size: 30.0,),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),


          Flexible(
              child: ListView.builder(
                reverse: true,
                controller: _scrollController,
                padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 0),
                itemCount: items.length,
                itemBuilder: (context, index) {
                  return Container(
                    padding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 10.0),
                    margin: EdgeInsets.only(top: 5.0, left: 20.0),
                    decoration: BoxDecoration(
                      border: Border(bottom: BorderSide(color: Colors.grey)),
                    ),
                    child: Text(
                      '${items[index]}',
                      style: TextStyle(
                        fontSize: 14.0,
                      ),
                    ),
                  );
                },
              )),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 10.0),
            decoration: BoxDecoration(
              color: Colors.grey.withOpacity(0.1),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              mainAxisSize: MainAxisSize.max,
              crossAxisAlignment: CrossAxisAlignment.end,
              children: <Widget>[
                RaisedButton(
                  color: btPressed ? Colors.brown : Colors.teal,
                  textColor: Colors.white70,
                  child: btPressed
                      ? Text('Start'.toUpperCase())
                      : Text('Pause'.toUpperCase()),
                  onPressed: () {
                    setState(() {
                      if(FlutterBluetoothSerial.DISCONNECTED != null){
                        MaterialPageRoute(builder: (context) => BluetoothRoute());
                      }
                      btPressed ^= true;

                      if (!btPressed && _schrittController.text.isNotEmpty && _anzahlController.text.isNotEmpty) {
                        //_connect();
                        bluetooth.write(createJSON());
                        print(createJSON());
                      } else {
                        btPressed = true;
                        bluetooth.write("{\"paused\":true}");
                      }
                      print(btPressed);
                    });
                  },
                ),
                RaisedButton(
                  color: Colors.brown,
                  textColor: Colors.white70,
                  child: Text('Stop'.toUpperCase()),
                  onPressed: () {
                    setState(() {
                      btPressed = true;
                      print(btPressed);
                      bluetooth.write("{\"stoped\":true}");
                      _anzahlController.text = "";
                      _schrittController.text = "";
                    });
                  },
                )
              ],
            ),
          ),
        ],
      ),
    );
  }

  String createJSON(){
    String json = "";
    switch(_value) {
      case "1.8":
        {
          json = "{\"anzahlBilder\":" + _anzahlController.text +
              ",\"paused\":false,\"stoped\":false,\"vollwinkel\":true,\"halbwinkel\":false,\"viertelwinkel\":false,\"achtelwinkel\":false,\"sechzehntelwinkel\":false,\"steps\":" + _schrittController.text + "}";
        }
        break;
      case "0.9":
        {
          json = "{\"anzahlBilder\":" + _anzahlController.text +
              ",\"paused\":false,\"stoped\":false,\"vollwinkel\":false,\"halbwinkel\":true,\"viertelwinkel\":false,\"achtelwinkel\":false,\"sechzehntelwinkel\":false,\"steps\":" + _schrittController.text + "}";
        }
        break;
      case "0.45":
        {
          json = "{\"anzahlBilder\":" + _anzahlController.text +
              ",\"paused\":false,\"stoped\":false,\"vollwinkel\":false,\"halbwinkel\":false,\"viertelwinkel\":true,\"achtelwinkel\":false,\"sechzehntelwinkel\":false,\"steps\":" + _schrittController.text + "}";
        }
        break;
      case "0.225":
        {
          json = "{\"anzahlBilder\":" + _anzahlController.text +
              ",\"paused\":false,\"stoped\":false,\"vollwinkel\":false,\"halbwinkel\":false,\"viertelwinkel\":false,\"achtelwinkel\":true,\"sechzehntelwinkel\":false,\"steps\":" + _schrittController.text + "}";
        }
        break;
      case "0.1125":
        {
          json = "{\"anzahlBilder\":" + _anzahlController.text +
              ",\"paused\":false,\"stoped\":false,\"vollwinkel\":false,\"halbwinkel\":false,\"viertelwinkel\":false,\"achtelwinkel\":false,\"sechzehntelwinkel\":true,\"steps\":" + _schrittController.text + "}";
        }
        break;
    }
    return json;
  }

  void _submit(text) {

  }
  void _increase(){
    setState(() {
      schritte += 1;
      print(schritte);
    });
  }
  void _decrease(){
    setState(() {
      schritte -= 1;
      print(schritte);
    });
  }
  Timer testtimer(String p) {
    var timer = Timer(Duration(seconds: 4), () {
      setState(() {
        if (items.length < 20) items.add(p);
        testtimer(p);
      });
    });
    return timer;
  }

  Future<void> reciveData() async {
    bluetooth.onRead().listen((data) {
      setState(() {
      print(data);
      output += data;
      print(output);
      if(output.contains("}")){
        items.add(output.substring(1, output.indexOf("}")));
        output = "";
      }
      });
      _scrollController.jumpTo(_scrollController.position.maxScrollExtent);
    });
  }

  void _connect() {

      bluetooth.isConnected.then((isConnected) {
        if (!isConnected) {
          bluetooth
              .connect(device)
              .timeout(Duration(seconds: 10))
              .catchError((error) {
          });
        }
      });

  }

}
