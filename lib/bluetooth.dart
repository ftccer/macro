import 'package:flutter/material.dart';
import 'chat.dart';
import 'justieren.dart';
import 'package:flutter_bluetooth_serial/flutter_bluetooth_serial.dart';


class BluetoothRoute extends StatelessWidget {
  // This is the root of your Application
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'bluetooth',
      home: DevicesListView(),
    );

  }
}
class DevicesListView extends StatefulWidget {
  State createState() => new DevicesListViewState();
}

class DevicesListViewState extends State<DevicesListView> {
  final List<Device> _devices = <Device>[];
  bool changedSub = false;
  FlutterBluetoothSerial bluetooth = FlutterBluetoothSerial.instance;

  // Define some variables, which will be required later
  List<BluetoothDevice> _devicesList = [];
  BluetoothDevice _device;
  bool _connected = false;
  bool _pressed = false;


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(

        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage("assets/animal-bee.jpg"),
          )
        ),
        child: Container(
          decoration: BoxDecoration(
            color: Colors.white70.withOpacity(0.7),
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[

              Center(
                child: Container(
                  padding: EdgeInsets.only(left: 40.0, top: 100.0, bottom: 20.0),
                  child: Row(
                    children: <Widget>[
                      Text("Bluetooth", style: TextStyle(color: Colors.white, fontSize: 28.0, height: 2.0),),
                    ],
                  ),
                  decoration: BoxDecoration(
                    color: Colors.brown.withOpacity(0.9),
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.symmetric(horizontal: 20.0),
                padding: EdgeInsets.only( top: 10.0),
                decoration: BoxDecoration(
                    border: Border(
//                    bottom: BorderSide(
//                        color: Colors.grey[600],
//                        style: BorderStyle.solid,
//                        width: 1.0
//                    )
                    )
                ),
                width: MediaQuery.of(context).size.width,
                child: Text(
                    'gekoppelte Geräte',
                    style: TextStyle(
                      color: Colors.brown,
                      fontSize: 14.0,
                      fontWeight: FontWeight.bold,
                    )
                ),
              ),
              Flexible(
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 8.0),
                  child: devicesList(),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget devicesList() {
    const Icon blueIcon = Icon(Icons.bluetooth);
    final List<String> eltList = [];
    for (int i = 0; i< _devicesList.length; i++)
      {
        eltList.add(_devicesList.elementAt(i).name);
      }
    return ListView(
      children: tileList(eltList),
    );
  }
  List<Card> tileList(List<String> list) {
    List<Card> listTiles = <Card>[];
    for(var i = 0; i < list.length; i++) {
      Card l = _createTile(list[i], "koppeln", i.toString(), i);
      listTiles.add(l);
    }
    return listTiles;
  }
  String check;
  int status = 0;
  Card _createTile(String title, String subtitle, String key, int index) {
    const Icon blueIcon = Icon(Icons.bluetooth, color: Colors.white,);


    return Card (
        color: Colors.brown.withOpacity(0.6),
        child: ListTile(
        leading: blueIcon,
        title: Text(title, style: TextStyle(color: Colors.white),),
        key: Key(key),
        trailing: ((key == check) && status == 1) ?
              Text("verbunden", style: TextStyle(color: Colors.greenAccent),) : null,
        onTap: () {
          if(key == check) {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => Setup(device: _device, bluetooth: bluetooth)),
            );
          }
          _device = _devicesList.elementAt(index);
          _connect();

          setState(() {
            if(_connected)
            check = key;
            status = 1;
          });

        }
    ));
  }

  @override
  void initState() {
    super.initState();
    bluetoothConnectionState();
  }

  void _connect() {
    if (_device == null) {
    } else {
      bluetooth.isConnected.then((isConnected) {
        if (!isConnected) {
          bluetooth
              .connect(_device)
              .timeout(Duration(seconds: 10))
              .catchError((error) {
            setState(() => _pressed = false);
          });
          setState(() => _pressed = true);
        }
      });
    }
  }

  // Method to disconnect bluetooth
  void _disconnect() {
    bluetooth.disconnect();
    setState(() => _pressed = true);
  }

  // We are using async callback for using await
  Future<void> bluetoothConnectionState() async {
    List<BluetoothDevice> devices = [];

    // To get the list of paired devices
    try {
      devices = await bluetooth.getBondedDevices();
    } on Exception {
      print("Error");
    }

    // For knowing when bluetooth is connected and when disconnected
    bluetooth.onStateChanged().listen((state) {
      switch (state) {
        case FlutterBluetoothSerial.CONNECTED:
          setState(() {
            _connected = true;
            _pressed = false;
          });

          break;

        case FlutterBluetoothSerial.DISCONNECTED:
          setState(() {
            _connected = false;
            _pressed = false;
          });
          break;

        default:
          print(state);
          break;
      }
    });

    // It is an error to call [setState] unless [mounted] is true.
    if (!mounted) {
      return;
    }

    // Store the [devices] list in the [_devicesList] for accessing
    // the list outside this class
    setState(() {
      _devicesList = devices;
    });
  }

}

class Device extends StatelessWidget {
  Device({this.name});
  final String name;
  @override
  Widget build(BuildContext context) {

    return Container(
        margin: EdgeInsets.symmetric(horizontal: 10.0),
        child: FlatButton(
          onPressed: () {
          }, // create bluetooth Connection
          textColor: Color(0x000000),
          color: Color(0x454545),
          child: Text(name),
        )
    );
  }
}
